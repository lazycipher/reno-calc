/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      keyframes: {
        rotateX3DFadeIn: {
          "0%": {
            opacity: 0,
            transform: "rotateX(-90deg)",
            "transform-origin": "bottom",
            perspective: "1000px",
          },
          "100%": {
            opacity: 1,
            transform: "rotateX(0)",
            "transform-origin": "bottom",
            perspective: "1000px",
          },
        },
        rotateX3DFadeOut: {
          "0%": {
            opacity: 1,
            transform: "rotateX(0)",
            "transform-origin": "bottom",
            perspective: "1000px",
          },
          "100%": {
            opacity: 0,
            transform: "rotateX(90deg)",
            "transform-origin": "bottom",
            perspective: "1000px",
          },
        },
      },
      animation: {
        rotateXFadeIn: "rotateX3DFadeIn 0.5s ease-in-out forwards",
        rotateXFadeOut: "rotateX3DFadeOut 0.5s ease-in-out forwards",
      },
    },
  },
  plugins: [require("daisyui")],
};
