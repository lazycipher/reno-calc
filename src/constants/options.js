export const booleanOptions = [
  {
    id: "tiles",
    label: "Tiles",
    percent_area: 30,
  },
  {
    id: "pool",
    label: "Pool",
    percent_area: 10,
  },
  {
    id: "seating",
    label: "Seating",
    percent_area: 15,
  },
  {
    id: "bar",
    label: "Bar",
    percent_area: 10,
  },
  {
    id: "oven",
    label: "Oven",
    percent_area: 100,
  },
  {
    id: "grill",
    label: "Grill",
    percent_area: 100,
  },
  {
    id: "fridge",
    label: "Fridge",
    percent_area: 100,
  },
  {
    id: "pergola",
    label: "Pergola",
    percent_area: 15,
  },
  {
    id: "tree",
    label: "Tree",
    percent_area: 25,
  },
  {
    id: "lighting",
    label: "Lighting",
    percent_area: 100,
  },
  {
    id: "artificial_grass",
    label: "Artificial Grass",
    percent_area: 70,
  },
];

export const budgetOptions = [
  {
    id: "economic",
    label: "Economic",
    cost: {
      tiles: {
        USD: 200,
        AED: 200,
      },
      pool: {
        USD: 3500,
        AED: 3500,
      },
      seating: {
        USD: 400,
        AED: 400,
      },
      bar: {
        USD: 400,
        AED: 400,
      },
      oven: {
        USD: 6000,
        AED: 6000,
      },
      grill: {
        USD: 5000,
        AED: 5000,
      },
      fridge: {
        USD: 1500,
        AED: 1500,
      },
      pergola: {
        USD: 500,
        AED: 500,
      },
      tree: {
        USD: 130,
        AED: 130,
      },
      lighting: {
        USD: 30,
        AED: 30,
      },
      artificial_grass: {
        USD: 50,
        AED: 50,
      },
    },
  },
  {
    id: "standard",
    label: "Standard",
    cost: {
      tiles: {
        USD: 300,
        AED: 300,
      },
      pool: {
        USD: 5250,
        AED: 5250,
      },
      seating: {
        USD: 600,
        AED: 600,
      },
      bar: {
        USD: 600,
        AED: 600,
      },
      oven: {
        USD: 9000,
        AED: 9000,
      },
      grill: {
        USD: 7500,
        AED: 7500,
      },
      fridge: {
        USD: 2250,
        AED: 2250,
      },
      pergola: {
        USD: 750,
        AED: 750,
      },
      tree: {
        USD: 195,
        AED: 195,
      },
      lighting: {
        USD: 45,
        AED: 45,
      },
      artificial_grass: {
        USD: 75,
        AED: 75,
      },
    },
  },
  {
    id: "highend",
    label: "High End",
    cost: {
      tiles: {
        USD: 450,
        AED: 450,
      },
      pool: {
        USD: 7875,
        AED: 7875,
      },
      seating: {
        USD: 900,
        AED: 900,
      },
      bar: {
        USD: 900,
        AED: 900,
      },
      oven: {
        USD: 13500,
        AED: 13500,
      },
      grill: {
        USD: 11250,
        AED: 11250,
      },
      fridge: {
        USD: 3375,
        AED: 3375,
      },
      pergola: {
        USD: 1125,
        AED: 1125,
      },
      tree: {
        USD: 293,
        AED: 293,
      },
      lighting: {
        USD: 68,
        AED: 68,
      },
      artificial_grass: {
        USD: 113,
        AED: 113,
      },
    },
  },
  {
    id: "superhighend",
    label: "Super High End",
    cost: {
      tiles: {
        USD: 675,
        AED: 675,
      },
      pool: {
        USD: 11813,
        AED: 11813,
      },
      seating: {
        USD: 1350,
        AED: 1350,
      },
      bar: {
        USD: 1350,
        AED: 1350,
      },
      oven: {
        USD: 20250,
        AED: 20250,
      },
      grill: {
        USD: 16875,
        AED: 16875,
      },
      fridge: {
        USD: 5063,
        AED: 5063,
      },
      pergola: {
        USD: 1687.5,
        AED: 1687.5,
      },
      tree: {
        USD: 439,
        AED: 439,
      },
      lighting: {
        USD: 101,
        AED: 101,
      },
      artificial_grass: {
        USD: 169,
        AED: 169,
      },
    },
  },
];

export const budgetValues = {
  economic: "economic",
  standard: "standard",
  highend: "highend",
  superhighend: "superhighend",
};

export const currencyOptions = {
  aed: "AED",
  usd: "USD",
};

export const booleanValues = {
  tiles: "tiles",
  pool: "pool",
  seating: "seating",
  bar: "bar",
  oven: "oven",
  grill: "grill",
  fridge: "fridge",
  pergola: "pergola",
  tree: "tree",
  lighting: "lighting",
  artificial_grass: "artificial_grass",
};

export const emiPlans = [
  {
    id: 3,
    label: "3 Months",
    installments: "3 Installments",
    markup: 5,
    downpayment: 30,
    movein: 10,
  },
  {
    id: 6,
    label: "6 Months",
    installments: "6 Installments",
    markup: 10,
    downpayment: 25,
    movein: 10,
  },
  {
    id: 12,
    label: "12 Months",
    installments: "12 Installments",
    markup: 15,
    downpayment: 10,
    movein: 10,
  },
];

export const emiPlansTenure = {
  3: 3,
  6: 6,
  12: 12,
};
