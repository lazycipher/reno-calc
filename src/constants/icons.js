import { GiPalmTree } from "react-icons/gi";
import { GiPoolTableCorner } from "react-icons/gi";
import { MdEventSeat } from "react-icons/md";
import { MdOutlineTableBar } from "react-icons/md";
import { PiOvenBold } from "react-icons/pi";
import { MdOutlineOutdoorGrill } from "react-icons/md";
import { RiFridgeLine } from "react-icons/ri";
import { FaArchway } from "react-icons/fa6";
import { FaSquare } from "react-icons/fa6";
import { GiCeilingLight } from "react-icons/gi";
import { GiHighGrass } from "react-icons/gi";
import { GiReceiveMoney } from "react-icons/gi";
import { GiTakeMyMoney } from "react-icons/gi";
import { GiMoneyStack } from "react-icons/gi";
import { GiCash } from "react-icons/gi";
import { TbCurrencyDirham } from "react-icons/tb";
import { BsCurrencyDollar } from "react-icons/bs";

export const optionIconMap = {
  tree: GiPalmTree,
  pool: GiPoolTableCorner,
  seating: MdEventSeat,
  bar: MdOutlineTableBar,
  oven: PiOvenBold,
  grill: MdOutlineOutdoorGrill,
  fridge: RiFridgeLine,
  pergola: FaArchway,
  tiles: FaSquare,
  lighting: GiCeilingLight,
  artificial_grass: GiHighGrass,
};

export const budgetValueIconMap = {
  economic: GiReceiveMoney,
  standard: GiTakeMyMoney,
  highend: GiMoneyStack,
  superhighend: GiCash,
};

export const currencyIconMap = {
  AED: TbCurrencyDirham,
  USD: BsCurrencyDollar,
};
