import React, { createContext, useContext, useReducer } from "react";
import { currencyOptions } from "../../constants/options";
import { calculateCost } from "../../helper/costCalculator";

export const actionTypes = {
  SET_CURRENCY: "SET_CURRENCY",
  SET_SIZE: "SET_SIZE",
  TOGGLE_FEATURE: "TOGGLE_FEATURE",
  SET_BUDGET: "SET_BUDGET",
  RESET: "RESET",
  TOGGLE_VIEW: "TOGGLE_VIEW",
  SET_TOTAL_COST: "SET_TOTAL_COST",
};

const initialState = {
  currency: currencyOptions.aed,
  show_form: false,
  size: "",
  budget: "",
  total_cost: null,
  features: {
    tiles: false,
    pool: false,
    seating: false,
    bar: false,
    oven: false,
    grill: false,
    fridge: false,
    pergola: false,
    tree: false,
    lighting: false,
    artificial_grass: false,
  },
};

const calculatorReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.RESET:
      return { ...initialState };
    case actionTypes.SET_CURRENCY:
      return { ...state, currency: action.payload };
    case actionTypes.SET_TOTAL_COST:
      return { ...state, total_cost: action.payload };
    case actionTypes.SET_SIZE:
      return { ...state, size: action.payload };
    case actionTypes.TOGGLE_VIEW:
      return { ...state, show_form: action.payload };
    case actionTypes.TOGGLE_FEATURE:
      return {
        ...state,
        features: {
          ...state.features,
          [action.payload]: !state.features[action.payload],
        },
      };
    case actionTypes.SET_BUDGET:
      return { ...state, budget: action.payload };
    default:
      return state;
  }
};

export const CalculatorContext = createContext();

const CalculatorProvider = ({ children }) => {
  const [state, dispatch] = useReducer(calculatorReducer, initialState);

  const handleCurrencyChange = (value) => {
    dispatch({ type: actionTypes.SET_CURRENCY, payload: value });
  };

  const handleReset = (value) => {
    dispatch({ type: actionTypes.RESET, payload: value });
  };

  const handleSizeChange = (value) => {
    dispatch({ type: actionTypes.SET_SIZE, payload: value });
  };

  const toggleView = (value) => {
    // dispatch({ type: actionTypes.TOGGLE_VIEW, payload: value });
    // Clears the total_cost to trigger view of result card
    dispatch({ type: actionTypes.SET_TOTAL_COST, payload: null });
  };

  const handleFeatureChange = (feature) => {
    dispatch({ type: actionTypes.TOGGLE_FEATURE, payload: feature });
  };

  const handleBudgetChange = (value) => {
    dispatch({ type: actionTypes.SET_BUDGET, payload: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const selectedFeatures = [];

    Object.keys(state.features).forEach((item) => {
      if (state.features[item]) {
        selectedFeatures.push(item);
      }
    });

    const calculatedCost = calculateCost(
      state.size,
      state.budget,
      state.currency,
      selectedFeatures
    );
    dispatch({ type: actionTypes.SET_TOTAL_COST, payload: calculatedCost });
  };

  return (
    <CalculatorContext.Provider
      value={{
        state,
        functions: {
          handleSizeChange,
          handleFeatureChange,
          handleBudgetChange,
          handleCurrencyChange,
          handleSubmit,
          handleReset,
          toggleView,
        },
      }}
    >
      {children}
    </CalculatorContext.Provider>
  );
};

export default CalculatorProvider;

export const useCalculator = () => useContext(CalculatorContext);
