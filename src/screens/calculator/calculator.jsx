import React from "react";
import Calculator from "../../components/Calculator";
import { useCalculator } from "./context";
import ResultCard from "../../components/Result";

const CalculatorScreen = () => {
  const { state, functions } = useCalculator();
  const { total_cost } = state;
  return !total_cost ? (
    <Calculator state={state} functions={functions} />
  ) : (
    <ResultCard state={state} functions={functions} />
  );
};

export default CalculatorScreen;
