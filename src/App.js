import CalculatorScreen from "./screens/calculator/calculator";
import CalculatorProvider from "./screens/calculator/context";

function App() {
  return (
    <div className="flex justify-center items-center p-8">
      <CalculatorProvider>
        <CalculatorScreen />
      </CalculatorProvider>
    </div>
  );
}

export default App;
