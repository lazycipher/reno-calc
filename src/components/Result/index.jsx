import React from "react";
import FeatureTile from "../Calculator/FeatureTile";
import { booleanOptions, budgetOptions } from "../../constants/options";
import BudgetTile from "../Calculator/BudgetTile";
import { GiCrossMark } from "react-icons/gi";
import Plans from "./Plans";

const ResultCard = ({ state, functions: { toggleView } }) => {
  const { size, features, budget, total_cost, currency } = state;
  return (
    <div className="max-w-4xl w-full shadow-xl rounded-xl flex flex-col gap-8 border py-8">
      <div className="w-full flex justify-center items-center px-4 mb-4 relative">
        <span className="font-bold self-center text-xl">
          We've got 3 plans for you
        </span>
        <button
          onClick={toggleView}
          className="btn btn-outline btn-secondary absolute right-8"
        >
          <GiCrossMark />
        </button>
      </div>
      <Plans totalCost={total_cost} currency={currency} />
      <div className="w-full p-4 flex flex-col gap-4">
        <div className="divider my-0"></div>

        <div className="flex flex-col gap-4 px-8">
          <span className="font-bold">Provided Area/Size</span>
          <div className="flex flex-wrap justify-center items-center border border-dashed border-primary rounded-xl h-36 w-56">
            <span className="text-primary font-bold">
              {size} m <sup>2</sup>
            </span>
          </div>
        </div>
        <div className="divider my-0"></div>

        <div className="flex flex-col gap-4 px-8">
          <span className="font-bold">Selected Features</span>
          <div className="flex flex-wrap gap-4">
            {booleanOptions.map((option) =>
              features[option.id] ? (
                <FeatureTile key={option.id} option={option} checked disabled />
              ) : null
            )}
          </div>
        </div>
        <div className="divider my-0"></div>
        <div className="flex flex-col gap-4 px-8">
          <span className="font-bold">Selected Budget</span>
          <div className="flex flex-wrap gap-4">
            {budgetOptions.map((option) =>
              budget === option.id ? (
                <BudgetTile key={option.id} option={option} checked disabled />
              ) : null
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ResultCard;
