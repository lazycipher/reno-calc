import React, { useMemo } from "react";
import { emiPlans, emiPlansTenure } from "../../constants/options";
import { currencyIconMap } from "../../constants/icons";

const Plans = ({ totalCost, currency }) => {
  const markup = useMemo(
    () => ({
      [emiPlansTenure[3]]: (emiPlans[0].markup * totalCost) / 100,
      [emiPlansTenure[6]]: (emiPlans[1].markup * totalCost) / 100,
      [emiPlansTenure[12]]: (emiPlans[2].markup * totalCost) / 100,
    }),
    [totalCost]
  );

  const downpayment = useMemo(
    () => ({
      [emiPlansTenure[3]]: (emiPlans[0].downpayment * totalCost) / 100,
      [emiPlansTenure[6]]: (emiPlans[1].downpayment * totalCost) / 100,
      [emiPlansTenure[12]]: (emiPlans[2].downpayment * totalCost) / 100,
    }),
    [totalCost]
  );

  const movein = useMemo(
    () => ({
      [emiPlansTenure[3]]: (emiPlans[0].movein * totalCost) / 100,
      [emiPlansTenure[6]]: (emiPlans[1].movein * totalCost) / 100,
      [emiPlansTenure[12]]: (emiPlans[2].movein * totalCost) / 100,
    }),
    [totalCost]
  );

  return (
    <div className="flex gap-4 justify-center items-center px-4">
      {emiPlans.map((plan) => (
        <div key={plan.id} className="stats stats-vertical shadow border">
          <div className="stat place-items-center">
            <div className="stat-title font-bold">Tenure</div>
            <div className="stat-value text-primary text-center">
              {plan.label}
            </div>
            <div className="stat-desc font-bold">{plan.installments}</div>
          </div>

          <div className="stat place-items-center">
            <div className="stat-title font-bold">Markup</div>
            <div className="stat-value flex">
              <CurrencyIcon currency={currency} size={20} />
              {markup[plan.id]}
            </div>
            <div className="stat-desc font-bold">{plan.markup}%</div>
          </div>

          <div className="stat place-items-center">
            <div className="stat-title font-bold">Downpayment</div>
            <div className="stat-value flex">
              <CurrencyIcon currency={currency} size={20} />
              {downpayment[plan.id]}
            </div>
            <div className="stat-desc font-bold">{plan.downpayment}%</div>
          </div>

          <div className="stat place-items-center">
            <div className="stat-title font-bold">Move In</div>
            <div className="stat-value flex">
              <CurrencyIcon currency={currency} size={20} />
              {movein[plan.id]}
            </div>
            <div className="stat-desc font-bold">{plan.movein}%</div>
          </div>

          <div className="stat place-items-center">
            <div className="stat-title font-bold">Total Cost</div>
            <div className="stat-value text-secondary  flex">
              <CurrencyIcon currency={currency} size={20} />
              {totalCost}
            </div>
            <div className="stat-desc font-bold">tentative</div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Plans;

const CurrencyIcon = ({ currency, size }) => {
  const IconComponent = currencyIconMap[currency];
  return IconComponent ? <IconComponent size={size} /> : null;
};
