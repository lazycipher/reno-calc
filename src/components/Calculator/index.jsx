import React, { useMemo } from "react";
import { TbCurrencyDirham } from "react-icons/tb";
import { BsCurrencyDollar } from "react-icons/bs";
import Inputs from "./Inputs";
import { currencyOptions } from "../../constants/options";

const Calculator = ({ state, functions }) => {
  const { handleCurrencyChange, handleSubmit, handleReset } = functions || {};
  const isValid = useMemo(
    () =>
      state.budget &&
      state.size &&
      Object.values(state.features).filter((item) => item).length,
    [state]
  );
  return (
    <div className="max-w-2xl shadow-xl rounded-xl flex flex-col gap-8 border">
      <div className="flex justify-center items-center w-full pt-8 px-8 relative">
        <p className="text-center text-xl font-bold grow">Reno - Calculator</p>
        <div className="flex items-center gap-2 absolute right-8">
          <span className="font-bold text-xs">Currency</span>
          <label className="swap swap-flip font-bold text-xl rounded-xl border p-2">
            <input
              type="checkbox"
              checked={state.currency === currencyOptions.aed}
              onChange={(e) =>
                handleCurrencyChange(
                  e.target.checked ? currencyOptions.aed : currencyOptions.usd
                )
              }
            />

            <div className="swap-on">
              <TbCurrencyDirham />
            </div>
            <div className="swap-off">
              <BsCurrencyDollar />
            </div>
          </label>
        </div>
      </div>
      <div className="divider my-0 py-0"></div>

      <Inputs state={state} functions={functions} />
      <div className="divider my-0"></div>
      <div className="flex justify-center gap-8 p-8 pt-0">
        <button
          onClick={handleSubmit}
          disabled={!isValid}
          className="btn btn-primary w-56"
        >
          Calculate
        </button>
        <button onClick={handleReset} className="btn btn-outline w-36">
          Reset
        </button>
      </div>
    </div>
  );
};

export default Calculator;
