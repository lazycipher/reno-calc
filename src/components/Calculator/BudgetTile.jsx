import React from "react";
import { budgetValueIconMap } from "../../constants/icons";
import { GiCheckMark } from "react-icons/gi";

const BudgetTile = ({ checked, option, handleBudgetChange, disabled }) => {
  return (
    <div
      className={`p-8 border rounded-xl hover:cursor-pointer hover:brightness-75 font-bold relative flex flex-col items-center justify-center gap-4 ${
        checked ? "border-primary text-primary" : ""
      } ${disabled && "hover:cursor-not-allowed  hover:brightness-100"}`}
      onClick={() => !disabled && handleBudgetChange?.(option.id)}
    >
      {checked && <GiCheckMark className="absolute top-2 right-2" />}
      <BudgetIcon budget={option.id} size={20} />
      {option.label}
    </div>
  );
};

export default BudgetTile;

const BudgetIcon = ({ budget, size }) => {
  const IconComponent = budgetValueIconMap[budget];
  return IconComponent ? <IconComponent size={size} /> : null;
};
