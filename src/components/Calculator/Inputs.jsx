import React from "react";
import { booleanOptions, budgetOptions } from "../../constants/options";
import FeatureTile from "./FeatureTile";
import BudgetTile from "./BudgetTile";

const Inputs = ({ state, functions }) => {
  const { handleSizeChange, handleFeatureChange, handleBudgetChange } =
    functions || {};
  const { size, features, budget } = state;
  return (
    <div className="flex flex-col gap-4">
      <div className="flex flex-col gap-2 px-8">
        <span className="font-bold">
          Size in m<sup>2</sup>
        </span>
        <input
          type="number"
          placeholder="Size in meter square"
          className="input input-bordered w-full max-w-xs"
          value={size}
          onChange={(e) => handleSizeChange(e.target.value)}
        />
      </div>
      <div className="divider"></div>
      <div className="flex flex-col gap-4 px-8">
        <span className="font-bold">Choose Options</span>
        <div className="flex flex-wrap gap-4">
          {booleanOptions.map((option) => (
            <FeatureTile
              key={option.id}
              option={option}
              checked={features[option.id]}
              handleFeatureChange={handleFeatureChange}
            />
          ))}
        </div>
      </div>
      <div className="divider"></div>
      <div className="flex flex-col gap-4 px-8">
        <span className="font-bold">Budget</span>
        <div className="flex flex-wrap gap-4">
          {budgetOptions.map((option) => (
            <BudgetTile
              key={option.id}
              option={option}
              checked={budget === option.id}
              handleBudgetChange={handleBudgetChange}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Inputs;
