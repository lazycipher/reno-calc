import React from "react";
import { optionIconMap } from "../../constants/icons";
import { GiCheckMark } from "react-icons/gi";

const FeatureTile = ({ checked, option, handleFeatureChange, disabled }) => {
  return (
    <div
      className={`p-8 border rounded-xl hover:cursor-pointer hover:brightness-75 font-bold relative flex flex-col items-center justify-center gap-4 ${
        checked ? "border-primary text-primary" : ""
      } ${disabled && "hover:cursor-not-allowed  hover:brightness-100"}`}
      onClick={() => !disabled && handleFeatureChange?.(option.id)}
    >
      {checked && <GiCheckMark className="absolute top-2 right-2" />}
      <FeatureIcon feature={option.id} size={20} />
      {option.label}
    </div>
  );
};

export default FeatureTile;

const FeatureIcon = ({ feature, size }) => {
  const IconComponent = optionIconMap[feature];
  return IconComponent ? <IconComponent size={size} /> : null;
};
