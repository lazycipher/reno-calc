import {
  booleanOptions,
  booleanValues,
  budgetOptions,
  currencyOptions,
} from "../constants/options";

export const calculateCost = (
  area,
  budget,
  currency = currencyOptions.aed,
  inputs = []
) => {
  const budgetCostData = budgetOptions.find((item) => item.id === budget).cost;
  let totalCost = 0;
  inputs.forEach((item) => {
    const percent_area = booleanOptions.find(
      (it) => it.id === item
    ).percent_area;

    let requiredQty;
    if (item === booleanValues.lighting) {
      requiredQty = Number(area);
    } else if (percent_area === 100) {
      requiredQty = 1;
    } else {
      requiredQty = (Number(area) * Number(percent_area)) / 100;
    }

    const cost = Number(budgetCostData[item][currency]) * requiredQty;
    totalCost += cost;
  });
  return totalCost;
};
